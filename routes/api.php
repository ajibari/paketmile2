<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'TesController@register');
Route::post('login', 'TesController@login');
Route::post('logout', 'TesController@logout');
Route::post('forget_password', 'TesController@password_forget');
Route::post('org_register', 'TesController@org_create');
Route::post('org_check', 'TesController@org_check');
Route::post('verification', 'TesController@verification');
Route::post('org_login', 'TesController@org_login');

Route::post('setting/role', 'Setting\RoleController@create')->middleware('auth2:api');
Route::get('setting/role', 'Setting\RoleController@read')->middleware('auth2:api');
Route::put('setting/role', 'Setting\RoleController@update')->middleware('auth2:api');
Route::delete('setting/role/{id}', 'Setting\RoleController@delete')->middleware('auth2:api');

Route::put('setting/permission', 'Setting\PermissionController@update')->middleware('auth2:api');
Route::get('setting/permission', 'Setting\PermissionController@read')->middleware('auth2:api');

Route::post('setting/user', 'Setting\UserController@create')->middleware('auth2:api');
Route::get('setting/user', 'Setting\UserController@read')->middleware('auth2:api');
Route::put('setting/user', 'Setting\UserController@update')->middleware('auth2:api');
Route::delete('setting/user/{id}', 'Setting\UserController@delete')->middleware('auth2:api');
Route::get('setting/user/object_list', 'Setting\UserController@object_list')->middleware('auth2:api');

Route::post('setting/group', 'Setting\GroupController@create')->middleware('auth2:api');
Route::get('setting/group', 'Setting\GroupController@read')->middleware('auth2:api');
Route::put('setting/group', 'Setting\GroupController@update')->middleware('auth2:api');
Route::delete('setting/group/{id}', 'Setting\GroupController@delete')->middleware('auth2:api');

Route::post('setting/location', 'Setting\LocationController@create')->middleware('auth2:api');
Route::get('setting/location', 'Setting\LocationController@read')->middleware('auth2:api');
Route::put('setting/location', 'Setting\LocationController@update')->middleware('auth2:api');
Route::delete('setting/location/{id}', 'Setting\LocationController@delete')->middleware('auth2:api');

Route::get('todo', 'TesController@getLogin');
Route::get('cek', 'TesController@cek');
Route::get('cekjson', 'TesController@cekAmbilDataJson');

Route::post('login2', 'TesController@login2');
Route::get('ceklogin', 'TesController@tes')->middleware('auth2:api');

Route::get('tes', 'Setting\UserController@tes');
Route::get('role_permission', 'Setting\RoleController@tes');
Route::get('tesaja', 'TesController@tesHasMany');
Route::put('change_password', 'Setting\OrganizationController@changePassword');

Route::prefix('v1')->group(function () {
    Route::get('cekjson', 'TesController@cek');
});

Route::prefix('v1')->group(function () {
    //TASK
    Route::post('task', 'TaskController@create');
    Route::get('task/{doc_reff_id}', 'TaskController@read');
    Route::put('task', 'TaskController@update');
    Route::delete('task', 'TaskController@delete');

    //WEBHOOK
    Route::post('master_hook', 'WebhookController@create');
    Route::delete('master_hook/{id}', 'WebhookController@delete');
    Route::get('master_hooks', 'WebhookController@list');
    Route::put('master_hook', 'WebhookController@update');

    //WEBHOOK CLIENT
    Route::post('hook', 'Setting\WebhookClientController@register');
    Route::delete('hook/{id}', 'Setting\WebhookClientController@unregister');
    Route::get('hooks', 'Setting\WebhookClientController@list');
    Route::put('hook', 'Setting\WebhookClientController@update');
});

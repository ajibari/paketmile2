<?php

return [
    'role_structure' => [
        'superadmin' => [
            'dashboard' => 'c,r,u,d',
            'worker' => 'c,r,u,d',
            'task' => 'c,r,u,d',
            'setting/user' => 'c,r,u,d',
            'setting/location' => 'c,r,u,d',
            'setting/task' => 'c,r,u,d',
            'setting/organization' => 'c,r,u,d',
        ],
        'admin' => [
            'dashboard' => 'c,r,u,d',
            'worker' => 'r,u',
            'setting/user' => 'c,r,u,d',
            'setting/location' => 'c,r,u,d',
            'setting/task' => 'c,r,u,d',
            'setting/organization' => 'c,r,u,d',
        ],
        'manager' => [
            'dashboard' => 'r,u',
            'worker' => 'c,r,u,d',
        ],
        'worker' => [
            'dashboard' => 'r',
        ],
    ],
    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u',
        ],
    ],
    'permissions_map' => [
        'c' => 'add',
        'r' => 'view',
        'u' => 'edit',
        'd' => 'delete',
    ],
];

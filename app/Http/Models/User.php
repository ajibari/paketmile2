<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class User extends Eloquent
{
    use LaratrustUserTrait;
    use Notifiable,HasApiTokens;

    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id', 'full_name', 'email', 'password', 'token', 'phone', 'location_id', 'group_id', 'role_id', 'avatar', 'status',
    ];

    protected $return_group = ['name', 'display_name', 'description', 'color'];
    protected $return_location = ['name', 'display_name', 'long_lat'];
    protected $return_role = ['name', 'display_name', 'description'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'token',
    ];

    protected $appends = ['groups', 'roles', 'locations'];

    public function organization()
    {
        return $this->belongsTo('App\Http\Models\Organization', 'organization_id', '_id');
    }

    public function role()
    {
        //unable to handle array relationship (multiple role) use getRolesAttribute instead
        return $this->belongsTo('App\Http\Models\Role', 'role_id', '_id');
    }

    public function group()
    {
        //unable to handle array relationship (multiple group) use getGroupsAttribute instead
        return $this->belongsTo('App\Http\Models\Group', 'group_id', '_id');
    }

    public function role_user()
    {
        //unused, use users collection instead
        return $this->hasMany('App\Http\Models\RoleUser', 'user_id', '_id');
    }

    public function getGroupsAttribute()
    {
        $groups = $this->getAttributeValue('group_id');
        $result = [];

        // return $groups;
        if (!is_null($groups)) {
            if (is_array($groups)) {
                $list = DB::collection('groups')->whereIn('_id', $groups)->get()->toArray();
            } else {
                $list = DB::collection('groups')->where('_id', $groups)->get()->toArray();
            }

            if (is_array($list)) {
                foreach ($list as $l) {
                    array_push($result, Arr::only($l, $this->return_group));
                }

                return $result;
            }
        }

        return null;
    }

    public function getRolesAttribute()
    {
        $roles = $this->getAttributeValue('role_id');
        $result = [];

        // return $groups;
        if (!is_null($roles)) {
            if (is_array($roles)) {
                $list = DB::collection('roles')->whereIn('_id', $roles)->get()->toArray();
            } else {
                $list = DB::collection('roles')->where('_id', $roles)->get()->toArray();
            }

            if (is_array($list)) {
                foreach ($list as $l) {
                    array_push($result, Arr::only($l, $this->return_role));
                }

                return $result;
            }
        }

        return null;
    }

    public function getLocationsAttribute()
    {
        $roles = $this->getAttributeValue('location_id');
        $result = [];

        // return $groups;
        if (!is_null($roles)) {
            if (is_array($roles)) {
                $list = DB::collection('locations')->whereIn('_id', $roles)->get()->toArray();
            } else {
                $list = DB::collection('locations')->where('_id', $roles)->get()->toArray();
            }

            if (is_array($list)) {
                foreach ($list as $l) {
                    array_push($result, Arr::only($l, $this->return_location));
                }

                return $result;
            }
        }

        return null;
    }
}

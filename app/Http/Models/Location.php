<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class Location extends Eloquent
{
    use TrackableTrait;

    protected $collections = 'locations';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name', 'display_name', 'organization_id', 'description', 'lng', 'lat', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by', 'alamat_ip',
    ];

    public function organization()
    {
        return $this->belongsTo('App\Http\Models\Organization', 'organization_id', '_id');
    }
}

<?php

namespace App\Http\Models;

use Laratrust\Models\LaratrustPermission;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class Permission extends LaratrustPermission
{
    use TrackableTrait;

    protected $primaryKey = '_id';
    protected $collection = 'permissions';
    protected $fillable = [
        'name', 'display_name', 'description', 'organization_id', 'created_at', 'updated_at',
    ];
}

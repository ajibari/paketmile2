<?php

namespace App\Http\Models;

use Laratrust\Traits\LaratrustRoleTrait;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class Role extends Eloquent
{
    use LaratrustRoleTrait;

    protected $primaryKey = '_id';
    protected $collection = 'roles';
    protected $fillable = [
        'name', 'display_name', 'description', 'organization_id', 'status', 'created_at', 'updated_at',
    ];
    protected $hidden = ['permission_id', 'organization_id'];

    protected $return = ['name', 'display_name', 'description'];

    protected $appends = ['total_user', 'available_menu', 'menu'];

    public function getPermissionsAttribute()
    {
        $permissions = $this->getAttributeValue('permission_id');
        $result = [];

        if (!is_null($permissions)) {
            $list = DB::collection('permissions')->whereIn('_id', $permissions)->get()->toArray();
            if (is_array($permissions)) {
                foreach ($list as $l) {
                    array_push($result, Arr::only($l, $this->return));
                }

                return $result;
            }
        }

        return null;
    }

    public function getTotalUserAttribute()
    {
        $role = $this->getAttributeValue('_id');
        $list = DB::collection('users')->where('role_id', $role)->get()->toArray();

        return count($list);
    }

    public function getMenu2Attribute()
    {
        $permissions = $this->getAttributeValue('permission_id');
        $result = [];
        $available = [];

        $list_action = ['add', 'view', 'edit', 'delete'];
        $i = 0;

        if (!is_null($permissions)) {
            $list = Permission::whereIn('_id', $permissions)->get()->toArray();

            if (is_array($permissions)) {
                foreach ($list as $l) {
                    $permit = explode(' ', $l['display_name']);
                    $result[$i]['name'] = $permit[count($permit) - 1];

                    foreach ($list_action as $action) {
                        $val = false;
                        if ($action == trim(strtolower($permit[0]))) {
                            $val = true;
                        }

                        $result[$i]['permissions'][$action] = $val;
                    }
                    ++$i;
                }

                return $result;
            }
        }

        return null;
    }

    public function getAvailableMenuAttribute()
    {
        $permissions = $this->getAttributeValue('permission_id');
        $result = [];
        $available = [];
        $i = 0;

        $available_permissions = Permission::whereNull('organization_id')->orderBy('_id', 'asc')->get()->toArray();

        $permit_init = explode(' ', $available_permissions[0]['display_name']);
        $available[0]['name'] = $permit_init[count($permit_init) - 1];

        foreach ($available_permissions as $permission) {
            $permit = explode(' ', $permission['display_name']);

            $count = count($available);
            $x = $count - 1;

            if (isset($available[$x]['name'])) {
                if ($available[$x]['name'] != $permit[count($permit) - 1]) {
                    $available[$x + 1]['name'] = $permit[count($permit) - 1];
                // $available[$x + 1]['permission_id'][0] = $permission['_id'];
                } else {
                    // $count_permission = count($available[$x]['permission_id']);
                    // $available[$x + 1]['permission_id'][$count_permission] = $permission['_id'];
                }
            } else {
                $available[$x]['name'] = $permit[count($permit) - 1];
                // $available[$x]['permission_id'][0] = $permission['_id'];
            }

            ++$i;
        }

        return $available;
    }

    public function getMenu3Attribute()
    {
        $permissions = $this->getAttributeValue('permission_id');
        $result = [];
        $available = [];

        $list_action = ['add', 'view', 'edit', 'delete'];
        $available_permissions = Permission::whereNull('organization_id')->orderBy('_id', 'asc')->get();
        $list_permissions = [];

        if (!empty($permissions)) {
            $list_permissions = Permission::whereIn('_id', $permissions)->get();
        }

        $i = 0;

        foreach ($available_permissions as $available) {
            $available_permit = explode(' ', $available['display_name']);

            $count_result = count($result);
            $count_permissions = count($list_permissions);
            $x = $count_result;
            $y = $count_permissions;

            if (isset($result[$x - 1]['name'])) {
                if ($result[$x - 1]['name'] != $available_permit[count($available_permit) - 1]) {
                    $result[$x]['name'] = $available_permit[count($available_permit) - 1];

                    if (!empty($list_permissions)) {
                        foreach ($list_permissions as $permit) {
                            if ($permit->_id === $available->_id) {
                                $own_permit = explode(' ', $permit->display_name);
                                foreach ($list_action as $action) {
                                    $val = false;
                                    if ($action === trim(strtolower($own_permit[0]))) {
                                        $val = true;
                                    }

                                    $result[$x]['permissions'][$action] = $val;
                                }
                            } else {
                                foreach ($list_action as $action) {
                                    $val = false;
                                    $result[$x]['permissions'][$action] = $val;
                                }
                            }
                        }
                    } else {
                        foreach ($list_action as $action) {
                            $val = false;
                            $result[$x]['permissions'][$action] = $val;
                        }
                    }
                } else {
                    if (!empty($list_permissions)) {
                        foreach ($list_permissions as $permit) {
                            if ($permit->_id === $available->_id) {
                                $own_permit = explode(' ', $permit->display_name);
                                foreach ($list_action as $action) {
                                    $val = 'false '.$permit->display_name;
                                    if ($action === trim(strtolower($own_permit[0]))) {
                                        $val = true;
                                    }

                                    $result[$x - 1]['permissions'][$action] = $val;
                                }
                            }
                        }
                    } else {
                        foreach ($list_action as $action) {
                            $val = false;
                            $result[$x - 1]['permissions'][$action] = $val;
                        }
                    }
                }
            } else {
                $result[$i]['name'] = $available_permit[count($available_permit) - 1];
                if (!empty($list_permissions)) {
                    if ($list_permissions[0]->_id == $available->_id) {
                        $own_permit = explode(' ', $list_permissions[0]->display_name);
                        foreach ($list_action as $action) {
                            $val = false;
                            if ($action == trim(strtolower($own_permit[0]))) {
                                $val = true;
                            }

                            $result[$i]['permissions'][$action] = $val;
                        }
                    } else {
                        foreach ($list_action as $action) {
                            $val = false;
                            $result[$i]['permissions'][$action] = $val;
                        }
                    }
                } else {
                    foreach ($list_action as $action) {
                        $val = false;
                        $result[$i]['permissions'][$action] = $val;
                    }
                }
            }

            ++$i;
        }

        return $result;
    }

    public function getMenuAttribute()
    {
        $permissions = $this->getAttributeValue('permission_id');
        $list_action = ['add', 'view', 'edit', 'delete'];

        $available_permissions = Permission::whereNull('organization_id')->orderBy('_id', 'asc')->get();
        $list_permissions = [];

        if (!empty($permissions)) {
            $list_permissions = Permission::whereIn('_id', $permissions)->get()->toArray();
        }

        $result = [];
        $no = 0;

        foreach ($available_permissions as $available) {
            $count_result = count($result);

            $available_permit = explode(' ', $available['display_name']);
            $available_permit_parent = $available_permit[1];

            if (isset($result[0]['name'])) {
                if ($available_permit_parent != $result[$count_result - 1]['name']) {
                    $result[$count_result]['name'] = $available_permit_parent;

                    //default value
                    foreach ($list_action as $action) {
                        $result[$count_result]['permissions'][$action] = false;
                    }

                    foreach ($list_permissions as $permission) {
                        $own_permit = explode(' ', $permission['display_name']);
                        $own_permit_parent = $own_permit[1];
                        $own_permit_action = $own_permit[0];

                        if (in_array(strtolower($own_permit_action), $list_action) && $own_permit_parent === $available_permit_parent) {
                            $result[$count_result]['permissions'][strtolower($own_permit_action)] = true;
                        }
                    }

                    if (in_array(false, $result[$count_result]['permissions'])) {
                        $result[$count_result]['permissions']['all'] = false;
                    } else {
                        $result[$count_result]['permissions']['all'] = true;
                    }
                }
            } else {
                //inisialisasi inject permission
                $result[$no]['name'] = $available_permit[count($available_permit) - 1];

                //default value
                foreach ($list_action as $action) {
                    $result[$no]['permissions'][$action] = false;
                }

                foreach ($list_permissions as $permission) {
                    $own_permit = explode(' ', $permission['display_name']);
                    $own_permit_parent = $own_permit[1];
                    $own_permit_action = $own_permit[0];

                    if (in_array(strtolower($own_permit_action), $list_action) && $own_permit_parent === $available_permit_parent) {
                        $result[$no]['permissions'][strtolower($own_permit_action)] = true;
                    }
                }

                if (in_array(false, $result[$count_result]['permissions'])) {
                    $result[$no]['permissions']['all'] = false;
                } else {
                    $result[$count_result]['permissions']['all'] = true;
                }
            }

            ++$no;
        }

        return $result;
    }
}

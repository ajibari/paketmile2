<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class WebhookClient extends Eloquent
{
    use TrackableTrait;

    protected $primaryKey = '_id';
    protected $collection = 'hook_clients';
    protected $fillable = [
        'hook_event_id', 'organization_id', 'target_url', 'status', 'created_at', 'updated_at',
    ];

    public function webhook_event()
    {
        return $this->belongsTo('App\Http\Models\Webhook', 'hook_event_id', '_id');
    }
}

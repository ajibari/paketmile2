<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use Illuminate\Support\Facades\DB;

class Group extends Eloquent
{
    use TrackableTrait;

    protected $primaryKey = '_id';
    protected $collection = 'groups';
    protected $fillable = [
        'name', 'display_name', 'description', 'color', 'organization_id', 'status', 'created_at', 'updated_at',
    ];
    protected $appends = ['total_user'];

    public function organization()
    {
        return $this->belongsTo('App\Http\Models\Organization', 'organization_id', '_id');
    }

    public function getTotalUserAttribute()
    {
        $role = $this->getAttributeValue('_id');
        $list = DB::collection('users')->where('role_id', $role)->get()->toArray();

        return count($list);
    }
}

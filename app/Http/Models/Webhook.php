<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class Webhook extends Eloquent
{
    use TrackableTrait;

    protected $primaryKey = '_id';
    protected $collection = 'hook_events';
    protected $fillable = [
        'name', 'display_name', 'description', 'status', 'created_at', 'updated_at',
    ];
}

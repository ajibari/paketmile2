<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RoleUser extends Eloquent
{
    protected $collection = 'role_user';

    protected $primaryKey = '_id';
    protected $fillable = ['role_id', 'user_id', 'user_type', 'organization_id'];
}

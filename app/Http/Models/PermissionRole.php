<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PermissionRole extends Eloquent
{
    protected $collection = 'permission_role';
}

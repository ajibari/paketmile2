<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class Organization extends Eloquent
{
    use TrackableTrait;

    protected $collection = 'organizations';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name', 'user', 'properties', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by', 'alamat_ip',
    ];
}

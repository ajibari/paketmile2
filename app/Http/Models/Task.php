<?php

namespace App\Http\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class Task extends Eloquent
{
    use TrackableTrait;

    protected $primaryKey = '_id';
    protected $collection = 'tasks';
    protected $fillable = [
        'name', 'content', 'status', 'created_at', 'updated_at',
    ];

    public function organization()
    {
        return $this->belongsTo('App\Http\Models\Organization', 'organization_id', '_id');
    }
}

<?php

namespace App\Http\Infrastructurs\Interfaces;

interface RepositoryInterface
{
    public function findAll($limit = null);

    public function findById($id);

    public function create($data);

    public function update($data);

    public function delete($id);

    public function softDelete($id);
}

<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use App\Http\Models\Role;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

class RoleRepository implements RepositoryInterface
{
    public $primaryKey;
    protected $organization_id;

    use TrackableTrait;

    public function __construct($organization_id = null)
    {
        $role = new Role();
        $this->primaryKey = $role->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function findAll($limit = null)
    {
        if (!$limit) {
            $data = Role::where('organization_id', $this->organization_id)->limit($limit)->get()->toArray();
        } else {
            $data = Role::where('organization_id', $this->organization_id)->get()->toArray();
        }

        $list_key = ['_id', 'name', 'display_name', 'status', 'total_user'];
        $result = [];

        if (is_array($data)) {
            foreach ($data as $key) {
                array_push($result, Arr::only($key, $list_key));
            }
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $result;

        return $response;
    }

    public function findAllWithPermission($limit = null)
    {
        if (!$limit) {
            $data = Role::where('organization_id', $this->organization_id)->limit($limit)->get()->toArray();
        } else {
            $data = Role::where('organization_id', $this->organization_id)->get()->toArray();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = Role::whereIn($this->primaryKey, $id)->get();
        } else {
            $data = Role::find($id);
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findAdminId($organization_id)
    {
        $findRole = Role::where('name', env('ROLE_ADMIN'))
                    ->where('organization_id', $organization_id)->first();

        if (!$findRole) {
            return false;
        }

        return $findRole->_id;
    }

    public function create($data)
    {
        try {
            $input = inputToLower($data);

            $rules = [
                'organization_id' => 'required',
                'name' => 'required',
                'display_name' => 'required',
            ];
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();
                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $findRole = $this->isAvailable($input);

            if (!$findRole) {
                $response['status'] = false;
                $response['message'] = env('DUPLICATE_ROLE');

                return $response;
            }

            $input['status'] = (int) env('STATUS_ACTIVE');
            $createRole = Role::create($input);

            $primaryKey = $this->primaryKey;

            $response['status'] = true;
            $response['property'] = [
                'primary_key' => $primaryKey,
                $primaryKey => $createRole->$primaryKey,
            ];
            $response['model'] = $createRole;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function update($data)
    {
        try {
            $rules = [
                '_id' => 'required',
                'name' => 'nullable',
                'display_name' => 'nullable',
                'description' => 'nullable',
                'organization_id' => 'nullable',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $model = new Role();
            $primaryKey = $model->getKeyName();
            $findRole = Role::find($data[$primaryKey]);
            $fillableKeys = is_null($model->getFillable()) ? [] : $model->getFillable();

            if (!$findRole) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            foreach ($fillableKeys as $k => $v) {
                if (isset($data[$v])) {
                    $findRole->$v = $data[$v];
                }
            }

            $save = $findRole->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findRole->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function updateRoleWithPermission($data)
    {
        try {
            $rules = [
                'role_id' => 'required',
                'permission_id' => 'nullable',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }
            $model = new Role();
            $findRole = Role::find($data['role_id']);

            $findRole->permission_id = $data['permission_id'];
            $save = $findRole->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findRole->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $delGroup = Role::find($id)->delete();

            if (!$delGroup) {
                $response['status'] = false;
                $response['message'] = env('DEL_FAILED');

                return $response;
            }

            $response['status'] = true;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($data)
    {
        try {
            $rules = [
                '_id' => 'required',
                'organization_id' => 'nullable',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $model = new Role();
            $primaryKey = $model->getKeyName();
            $findRole = Role::find($data[$primaryKey]);

            if (!$findRole) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $findRole->status = (int) env('STATUS_DELETE');

            $save = $findRole->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findRole->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    protected function isAvailable($data)
    {
        $findRole = Role::where('name', $data['name'])
                    ->where('organization_id', $data['organization_id'])
                    ->first();

        if ($findRole) {
            return false;
        }

        return true;
    }

    public function createDefaultRole()
    {
        //create default role
        $default_roles = explode(',', env('DEFAULT_ROLE'));
        $i = 0;

        foreach ($default_roles as $role) {
            $data[$i]['name'] = $role;
            $data[$i]['display_name'] = $role;
            $data[$i]['description'] = ucwords($role);
            $data[$i]['organization_id'] = $this->organization_id;
            $data[$i]['status'] = 1;
            ++$i;
        }

        Role::insert($data);

        return true;
    }
}

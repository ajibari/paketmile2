<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use App\Http\Models\Organization;
use Illuminate\Support\Facades\Validator;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class OrgRepository implements RepositoryInterface
{
    use TrackableTrait;

    protected $primaryKey;
    protected $organization_id;

    public function __construct($organization_id = null)
    {
        $org = new Organization();
        $this->primaryKey = $org->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function primaryKey()
    {
        return $this->primaryKey;
    }

    public function findAll($limit = null)
    {
        if (!$limit) {
            $data = Organization::limit($limit)->get();
        } else {
            $data = Organization::all();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $data;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = Organization::whereIn($this->primaryKey, $id)->get();
        } else {
            $data = Organization::find($id);
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findByName($name)
    {
        $data = Organization::where('name', $name)
                ->where('status', 1)
                ->first();

        if (!$data) {
            return false;
        }

        return $data;
    }

    public function create($data)
    {
        /*
         * disini nanti dilampirkan mandatory untuk data User itu apa aja dan di validate sebelum di insert
         */

        try {
            $rules = [
                'org_name' => 'required|alpha_dash',
                'user' => 'required',
            ];
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();
                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            //cek apakah email sudah pernah digunakan jadi admin ?
            $findEmail = Organization::where('user.email', $data['user']['email'])->first();
            if ($findEmail) {
                $response['status'] = false;
                $response['message'] = env('DUPLICATE_EMAIL');

                return $response;
            }

            //cek apakah nama organisasi sudah digunakan ?
            $findOrgName = Organization::where('name', $data['org_name'])->first();
            if ($findOrgName) {
                $response['status'] = false;
                $response['message'] = env('DUPLICATE_NAME');

                return $response;
            }

            $primaryKey = $this->primaryKey;
            $data['name'] = $data['org_name'];
            $data['user']['activation_code'] = md5(uniqid($data['user']['email'], true));
            $org = Organization::create($data);
            $result['status'] = true;
            $result['property'] = [$primaryKey => $org->$primaryKey, 'activation_code' => $data['user']['activation_code']];
            $result['model'] = $org;
        } catch (\Exception $e) {
            die($e);
        }

        return $result;
    }

    public function update($data)
    {
        try {
            $org = new Organization();
            $primaryKey = $org->getKeyName();

            $orgData = $org::find($data[$primaryKey]);
            $fillable_keys = is_null($org->getFillable()) ? [] : $org->getFillable();

            //if password is set
            if (!empty($data['user']['password'])) {
                $data['user']['password'] = bcrypt($data['user']['password']);
            }

            if (is_array($fillable_keys)) {
                array_push($fillable_keys, $primaryKey);
            } else {
                $fillable_keys = array($fillable_keys, $primaryKey);
            }

            $primaryKeyVal = $data[$primaryKey];
            unset($data[$primaryKey]);

            foreach ($fillable_keys as $f => $v) {
                if (isset($data[$v])) {
                    if (isset($orgData->$v)) {
                        if (is_array($orgData->$v)) {
                            $e = $orgData->$v;
                            foreach ($data[$v] as $f2 => $v2) {
                                $e[$f2] = $v2;
                            }
                            $orgData->$v = $e;
                        } else {
                            $orgData->$v = $data[$v];
                        }
                    }
                }
            }

            $updated = $orgData->toArray();
            $save = $orgData->save();

            if (!$save) {
                $response['status'] = false;
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            if (empty($updated)) {
                $response['status'] = true;
                $response['property'] = '';

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $orgData->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function replace($data)
    {
        //this function is replace data, different with update
        try {
            $org = new Organization();
            $primaryKey = $org->getKeyName();

            $orgData = $org::find($data[$primaryKey]);
            $fillable_keys = is_null($org->getFillable()) ? [] : $org->getFillable();

            if (is_array($fillable_keys)) {
                array_push($fillable_keys, $primaryKey);
            } else {
                $fillable_keys = array($fillable_keys, $primaryKey);
            }

            $primaryKeyVal = $data[$primaryKey];
            unset($data[$primaryKey]);

            foreach ($fillable_keys as $f => $v) {
                if (isset($data[$v])) {
                    $orgData->$v = $data[$v];
                }
            }

            $updated = $orgData->getDirty();
            $save = $orgData->save();

            if (!$save) {
                return false;
            }

            if (empty($updated)) {
                return $data;
            }

            return $updated;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($id)
    {
        try {
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function isAvailable($data)
    {
        $rules = [
                'org_name' => 'required|alpha_dash',
            ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            $response['status'] = 'error';
            $response['message'] = $error;

            return $response;
        }

        $data = Organization::where('name', $data['org_name'])->first();

        if (!empty($data)) {
            $response['status'] = false;
            $response['message'] = env('ORG_NOT_AVAILABLE');

            return $response;
        }

        $response['status'] = true;

        return $response;
    }

    public function isValid($data)
    {
        $data = Organization::where('name', $data['org_name'])
            ->where('status', 1)->first();

        if (!empty($data)) {
            return true;
        }

        return false;
    }

    public function changeStatus()
    {
        try {
            $this->update($data);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function verification($email, $activation_code)
    {
        $data = Organization::where('user.email', $email)
            ->where('user.activation_code', $activation_code)
            ->where('status', 0)
            ->first();

        if (empty($data)) {
            return false;
        }

        return $data->toArray();
    }

    public function changePassword($password)
    {
        if (is_null($this->organization_id)) {
            $response['status'] = false;
            $response['message'] = env('UNAUTHORIZED');

            return $response;
        }

        $data = [
                '_id' => $this->organization_id,
                'user' => [
                    'password' => $password,
                ],
            ];

        return $this->update($data);
    }
}

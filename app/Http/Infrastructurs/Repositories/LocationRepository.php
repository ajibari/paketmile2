<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Models\Location;

class LocationRepository implements RepositoryInterface
{
    public $primaryKey;
    protected $organization_id;

    use TrackableTrait;

    public function __construct($organization_id = null)
    {
        $Location = new Location();
        $this->primaryKey = $Location->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function findAll($limit = null)
    {
        if (!$limit) {
            $data = Location::where('organization_id', $this->organization_id)->with('organization')->limit($limit)->get();
        } else {
            $data = Location::where('organization_id', $this->organization_id)->with('organization')->all();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = Location::where('organization_id', $this->organization_id)
                    ->whereIn($this->primaryKey, $id)->get();
        } else {
            $data = Location::where('organization_id', $this->organization_id)
                    ->where($this->primaryKey, $id)->first();
        }

        return $data;
    }

    public function create($data)
    {
        /*
         * validasi input
         * checking group name whether is exist
         * insert data
         */

        try {
            $input = inputToLower($data);

            $findLocation = Location::where('lat', $input['lat'])
                ->where('lng', $input['lng'])
                ->where('organization_id', $this->organization_id)
                ->first();

            if ($findLocation) {
                $response['status'] = false;
                $response['error_type'] = 'duplicate_location';
                $response['message'] = env('DUPLICATE_LOCATION');

                return $response;
            }

            $input['status'] = (int) env('STATUS_ACTIVE');
            $input['organization_id'] = $this->organization_id;

            $createLocation = Location::create($input);

            $primaryKey = $this->primaryKey;
            $response['status'] = true;
            $response['property'] = [
                'primary_key' => $primaryKey,
                $primaryKey => $createLocation->$primaryKey,
            ];
            $response['model'] = $createLocation;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function update($data)
    {
        try {
            $model = new Location();
            $primaryKey = $model->getKeyName();
            $findLocation = Location::where('organization_id', $this->organization_id)
                            ->where($this->primaryKey, $data[$primaryKey])->first();
            $fillableKeys = is_null($model->getFillable()) ? [] : $model->getFillable();

            if (!$findLocation) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $data['organization_id'] = $this->organization_id;

            foreach ($fillableKeys as $k => $v) {
                if (isset($data[$v])) {
                    $findLocation->$v = $data[$v];
                }
            }

            $save = $findLocation->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findLocation->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $delLocation = Location::where('organization_id', $this->organization_id)
                        ->where($this->primaryKey, $id)
                        ->delete();

            if (!$delLocation) {
                $response['status'] = false;
                $response['message'] = env('DEL_FAILED');

                return $response;
            }

            $response['status'] = true;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($id)
    {
    }
}

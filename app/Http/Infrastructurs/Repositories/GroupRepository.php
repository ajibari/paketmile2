<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use Illuminate\Support\Facades\Validator;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Models\Group;

class GroupRepository implements RepositoryInterface
{
    use TrackableTrait;

    protected $primaryKey;
    protected $organization_id;

    public function __construct($organization_id = null)
    {
        $Group = new Group();
        $this->primaryKey = $Group->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function findAll($limit = null)
    {
        if (!$limit) {
            $data = Group::where('organization_id', $this->organization_id)->with('organization')->limit($limit)->get();
        } else {
            $data = Group::where('organization_id', $this->organization_id)->with('organization')->all();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = Group::whereIn($this->primaryKey, $id)->get();
        } else {
            $data = Group::find($id);
        }

        return $data;
    }

    public function findByName($name)
    {
    }

    public function create($data)
    {
        /*
         * validasi input
         * checking group name whether is exist
         * insert data
         */

        try {
            $input = inputToLower($data);

            $rules = [
                'name' => 'required',
                'display_name' => 'required',
                'description' => 'required',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();
                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $findGroup = Group::where('name', $input['name'])
                        ->where('organization_id', $this->organization_id)
                        ->first();

            if ($findGroup) {
                $response['status'] = false;
                $response['error_type'] = 'duplicate_group';
                $response['message'] = env('DUPLICATE_GROUP');

                return $response;
            }

            $input['status'] = (int) env('STATUS_ACTIVE');
            $input['organization_id'] = $this->organization_id;
            $createGroup = Group::create($input);

            $primaryKey = $this->primaryKey;

            $response['status'] = true;
            $response['property'] = [
                                        'primary_key' => $primaryKey,
                                        $primaryKey => $createGroup->$primaryKey,
                                    ];
            $response['model'] = $createGroup;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function update($data)
    {
        try {
            $rules = [
                '_id' => 'required',
                'name' => 'nullable',
                'display_name' => 'nullable',
                'description' => 'nullable',
                'organization_id' => 'nullable',
                'color' => 'nullable',
                'status' => 'nullable',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $model = new Group();
            $primaryKey = $model->getKeyName();

            $findGroup = Group::where($primaryKey, $data[$primaryKey])
                        ->where('organization_id', $this->organization_id)->first();

            $fillableKeys = is_null($model->getFillable()) ? [] : $model->getFillable();

            if (!$findGroup) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            foreach ($fillableKeys as $k => $v) {
                if (isset($data[$v])) {
                    $findGroup->$v = $data[$v];
                }
            }

            $save = $findGroup->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findGroup->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function replace($data)
    {
    }

    public function delete($id)
    {
        try {
            $delGroup = Group::where('organization_id', $this->organization_id)
                        ->where('_id', $id)->delete();

            if (!$delGroup) {
                $response['status'] = false;
                $response['message'] = env('DEL_FAILED');

                return $response;
            }

            $response['status'] = true;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($id)
    {
    }

    public function tes(Request $request)
    {
        dd($this->header($request));
    }
}

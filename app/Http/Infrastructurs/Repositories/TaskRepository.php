<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\FirestoreApiClient;     //unofficial library
use App\Http\Infrastructurs\FirestoreDocument;      //unofficial library
use Google\Cloud\Firestore\FirestoreClient;

class TaskRepository
{
    private $private;
    private $organization_id;

    public function __construct($organization_id)
    {
        $this->organization_id = $organization_id;

        $config = [
            'projectId' => env('FIRESTORE_CLIENT_ID'),
            'keyFile' => json_decode(file_get_contents(base_path('key').'/'.env('FIRESTORE_KEY_JSON')), true),
            'keyu' => env('FIRESTORE_KEY_JSON'),
        ];

        $this->firestore = new FirestoreClient($config);
    }

    public function create($col, $doc, $content)
    {
        $docRef = $this->firestore->collection($col)->document($doc);
        $saveData = $docRef->set($content);

        if (!$saveData) {
            $response['status'] = false;
            $response['message'] = env('SAVE_FAILED');

            return $response;
        }

        $response['status'] = true;

        return $response;
    }

    public function read($doc_reff_id)
    {
        $collectionReff = $this->firestore->collection('task');
        $findData = $collectionReff->where('type', '=', $doc_reff_id);
        $documents = $findData->select(['type', 'status', 'order'])->documents();
        foreach ($documents as $document) {
            if ($document->exists()) {
                echo $document['type'];
                dd($document);
            }
        }
    }

    public function update($input)
    {
        $collection = env('FIRESTORE_COL_TASK');
        $data = $input;

        if (!empty($input['taskId'])) {
            $document = $this->firestore->document($collection.'/'.$data['taskId']);
            $snapshot = $document->snapshot();
            $userVar = $snapshot['UserVar'];

            foreach ($data as $k => $v) {
                if ($k == 'UserVar') {
                    foreach ($userVar as $k1 => $v1) {
                        $value = [$k1 => $v1];
                        if (!isset($data[$k][$k1])) {
                            $input_data[] = ['path' => 'UserVar', 'value' => $value];
                        }
                    }
                    foreach ($data[$k] as $k2 => $v2) {
                        $value_user_var = [$k2 => $v2];
                        $input_data[] = ['path' => 'UserVar', 'value' => $value_user_var];
                    }
                } else {
                    $input_data[] = ['path' => $k, 'value' => $v];
                }
            }

            $document->update($input_data);

            $document = $this->firestore->document($collection.'/'.$data['taskId']);
            $snapshot = $document->snapshot();

            $response['status'] = true;
            $response['property'] = json_decode(json_encode($snapshot->data(), JSON_UNESCAPED_SLASHES), true);
        } else {
            $findDocument = $this->firestore->collection($collection);
            $query = $findDocument->where('taskRefId', '=', $input['taskRefId']);

            $result = $query->documents();
            $rows = $result->rows();

            if (!$rows) {
                $response['status'] = false;

                return $response;
            }

            $id = $rows[0]->id();
            $document = $this->firestore->document($collection.'/'.$id);
            $snapshot = $document->snapshot();
            $userVar = $snapshot['UserVar'];

            foreach ($data as $k => $v) {
                if ($k == 'UserVar') {
                    foreach ($userVar as $k1 => $v1) {
                        $value = [$k1 => $v1];
                        if (!isset($data[$k][$k1])) {
                            $input_data[] = ['path' => 'UserVar', 'value' => $value];
                        }
                    }
                    foreach ($data[$k] as $k2 => $v2) {
                        $value_user_var = [$k2 => $v2];
                        $input_data[] = ['path' => 'UserVar', 'value' => $value_user_var];
                    }
                } else {
                    $input_data[] = ['path' => $k, 'value' => $v];
                }
            }

            $document->update($input_data);

            $snapshot = $document->snapshot();

            $response['status'] = true;
            $response['property'] = json_decode(json_encode($snapshot->data(), JSON_UNESCAPED_SLASHES), true);
        }

        return $response;
    }

    public function delete($input)
    {
        $collection = env('FIRESTORE_COL_TASK');
        $data = $input;

        if (!empty($input['taskId'])) {
            $document = $this->firestore->document($collection.'/'.$data['taskId']);

            $input_data = [
                ['path' => 'status', 'value' => 'deleted'],
                ['path' => 'deletedAt', 'value' => date('Y-m-d H:i:s')],
            ];
            $document->update($input_data);

            $document = $this->firestore->document($collection.'/'.$data['taskId']);
            $snapshot = $document->snapshot();

            $response['status'] = true;
            $response['property'] = json_decode(json_encode($snapshot->data(), JSON_UNESCAPED_SLASHES), true);
        } else {
            $findDocument = $this->firestore->collection($collection);
            $query = $findDocument->where('taskRefId', '=', $input['taskRefId']);

            $result = $query->documents();
            $rows = $result->rows();

            $input_data = [
                ['path' => 'status', 'value' => 'deleted'],
                ['path' => 'deletedAt', 'value' => date('Y-m-d H:i:s')],
            ];

            $id = $rows[0]->id();
            $document = $this->firestore->document($collection.'/'.$id);
            $document->update($input_data);

            $snapshot = $document->snapshot();

            $response['status'] = true;
            $response['property'] = json_decode(json_encode($snapshot->data(), JSON_UNESCAPED_SLASHES), true);
        }

        return $response;
    }

    public function deletePermanent($input)
    {
        $collection = env('FIRESTORE_COL_TASK');
        $data = $input;

        if (!empty($input['taskRefId'])) {
            $document = $this->firestore->document($collection.'/'.$data['taskRefId']);
            $document->delete();

            $response['status'] = true;
        } else {
            $findDocument = $this->firestore->collection($collection);
            $query = $findDocument->where('taskId', '=', $input['taskId']);

            $result = $query->documents();
            $rows = $result->rows();
            $id = $rows[0]->id();
            $document = $this->firestore->document($collection.'/'.$id);
            $document->delete();

            $response['status'] = true;
        }

        return $response;
    }

    public function createUnofficial($col, $doc, $data)
    {
        // dd($data);
        $client_id = 'paketidv2';
        $key = 'AIzaSyDDHFCHAH7Jth8NL3iwRzT8IQq1RYLcMWY';

        $firestore = new FireStoreApiClient($client_id, $key);
        $document = new FirestoreDocument();

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_array($v)) {
                    $obj = $this->setObject($v);
                    $document->setMap($k, $obj);

                // $document->setArray($k2, );
                    // }
                } else {
                    // $result[$k] = $v;
                    $document->setString($k, $v);
                }

                $firestore->updateDocument($col, $doc, $document);
            }
        }

        return true;
    }

    public function create2Unofficial($col, $doc, $data)
    {
        // dd($data);
        $client_id = 'paketidv2';
        $key = 'AIzaSyDDHFCHAH7Jth8NL3iwRzT8IQq1RYLcMWY';

        $firestore = new FireStoreApiClient($client_id, $key);
        $document = new FirestoreDocument();
        // $doc_people = $firestore->getDocument('people', 'mes');
        // dd($doc_people);
        // $document->setMap('resultoptions', $data);

        // dd($data);

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_array($v)) {
                    // $obj['fields']['tesObject'] = [
                    //     'mapValue' => ['fields' => ['tes' => ['stringValue' => 'tesd']]],
                    // ];

                    // $obj['fields']['tesObject2'] = [
                    //     'arrayValue' => ['stringValue' => 'tesd'],
                    // ];

                    // $document->setMap($k, $obj);

                    foreach ($data as $k => $v) {
                        // dd($k);
                        if (is_array($v)) {
                            foreach ($v as $k2 => $v2) {
                                if (is_array($v2)) {
                                    foreach ($v2 as $k3 => $v3) {
                                        $arr = [];

                                        if (is_array($v2)) {
                                            foreach ($v2 as $k4 => $v4) {
                                                foreach ($v4 as $k5 => $v5) {
                                                    $arr['values'][] = ['mapValue' => ['fields' => [$k5 => ['stringValue' => $v5]]]];
                                                }
                                            }

                                            $document->setArray($k2, $arr);
                                        }
                                    }
                                } else {
                                }
                            }
                        } else {
                            $arr['fields'][$k] = ['stringValue' => $v];
                        }
                    }
                } else {
                    $document->setString($k, $v);
                }

                $firestore->updateDocument($col, $doc, $document);
            }
        }

        return true;
    }

    protected function setObject($data)
    {
        // dd($data);
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if (is_array($v2)) {
                        // dd($v2);
                        foreach ($v2 as $k3 => $v3) {
                            $obj['fields'][$k] = [
                                'mapValue' => ['fields' => [$k3 => ['stringValue' => $v3]]],
                            ];
                        }
                    } else {
                    }
                }
            } else {
                $obj['fields'][$k] = ['stringValue' => $v];
            }
        }

        return $obj;
    }

    protected function setObject2($data)
    {
        // dd($data);
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if (is_array($v2)) {
                        // dd($v2);
                        foreach ($v2 as $k3 => $v3) {
                            $arr = [];
                            $arr['values'][] = ['stringValue' => ''];

                            $obj['fields'][$k] = [
                                'mapValue' => ['fields' => [$k3 => ['stringValue' => $v3]]],
                            ];
                        }
                    } else {
                    }
                }
            } else {
                $obj['fields'][$k] = ['stringValue' => $v];
            }
        }

        return $obj;
    }
}

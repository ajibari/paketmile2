<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Models\WebhookClient;

class WebhookClientRepository implements RepositoryInterface
{
    use TrackableTrait;
    protected $primaryKey;
    protected $organization_id;

    public function __construct($organization_id = null)
    {
        $webhook_client = new WebhookClient();
        $this->primaryKey = $webhook_client->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function findAll($limit = null)
    {
        if (!$limit) {
            $data = WebhookClient::where('organization_id', $this->organization_id)->limit($limit)->get();
        } else {
            $data = WebhookClient::where('organization_id', $this->organization_id)->all();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = WebhookClient::whereIn($this->primaryKey, $id)->get();
        } else {
            $data = WebhookClient::find($id);
        }

        return $data;
    }

    public function getAllHooks()
    {
    }

    public function create($input)
    {
        $find_webhookclient = WebhookClient::orWhere('hook_event_id', $input['hook_event_id'])
            ->orWhere('target_url', $input['target_url'])
            ->where('organization_id', $this->organization_id)
            ->first();

        if ($find_webhookclient) {
            $response['status'] = false;
            $response['error_type'] = 'duplicate_webhook';
            $response['message'] = env('DUPLICATE_WEBHOOK');

            return $response;
        }

        $input['status'] = (int) env('STATUS_ACTIVE');
        $input['organization_id'] = $this->organization_id;
        $create_webhook = WebhookClient::create($input);

        $primaryKey = $this->primaryKey;

        $response['status'] = true;
        $response['property'] = [
            'primary_key' => $primaryKey,
            $primaryKey => $create_webhook->$primaryKey,
        ];

        return $response;
    }

    public function update($data)
    {
        try {
            $model = new WebhookClient();
            $primaryKey = $this->primaryKey;

            $findWebhook = WebhookClient::where($primaryKey, $data[$primaryKey])
                ->where('organization_id', $this->organization_id)->first();

            $fillableKeys = is_null($model->getFillable()) ? [] : $model->getFillable();

            foreach ($fillableKeys as $k => $v) {
                if (isset($data[$v])) {
                    $findWebhook->$v = $data[$v];
                }
            }

            $save = $findWebhook->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findWebhook->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $del_webhook = WebhookClient::where('organization_id', $this->organization_id)
                ->where('_id', $id)->delete();

            if (!$del_webhook) {
                $response['status'] = false;
                $response['message'] = env('DEL_FAILED');

                return $response;
            }

            $response['status'] = true;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($id)
    {
    }
}

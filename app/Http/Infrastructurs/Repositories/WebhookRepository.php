<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use App\Http\Models\Webhook;
use App\Http\Infrastructurs\Traits\TrackableTrait;

class WebhookRepository implements RepositoryInterface
{
    public $primaryKey;
    protected $organization_id;

    use TrackableTrait;

    public function __construct($organization_id = null)
    {
        $webhook = new Webhook();
        $this->primaryKey = $webhook->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function findAll($limit = null)
    {
        $data = Webhook::where('organization_id', $this->organization_id)->limit($limit)->get()->toArray();

        $response['status'] = true;
        $response['collection'] = $data;

        return $response;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = Webhook::whereIn($this->primaryKey, $id)->get();
        } else {
            $data = Webhook::find($id);
        }

        return $data;
    }

    public function getAllHooks()
    {
        $data = Webhook::get()->toArray();

        $response['status'] = true;
        $response['collection'] = $data;

        return $response;
    }

    public function create($data)
    {
        try {
            $input = inputToLower($data);

            $input['status'] = (int) env('STATUS_ACTIVE');
            $input['organization_id'] = $this->organization_id;

            //belum ada validasi end point hook dari sisi client
            $createWebhook = Webhook::create($input);

            if (!$createWebhook) {
                $response['status'] = false;
                $response['message'] = env('CREATE_FAILED');

                return $response;
            }

            $primaryKey = $this->primaryKey;

            $response['status'] = true;
            $response['property'] = [
                'primary_key' => $primaryKey,
                $primaryKey => $createWebhook->$primaryKey,
            ];

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function update($data)
    {
        try {
            $model = new Webhook();
            $primaryKey = $this->primaryKey;

            $findWebhook = Webhook::where($primaryKey, $data[$primaryKey])
                ->where('organization_id', $this->organization_id)->first();

            $fillableKeys = is_null($model->getFillable()) ? [] : $model->getFillable();

            if (!$findWebhook) {
                $response['status'] = false;
                $response['property'] = null;
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            foreach ($fillableKeys as $k => $v) {
                if (isset($data[$v])) {
                    $findWebhook->$v = $data[$v];
                }
            }

            $save = $findWebhook->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = null;
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findWebhook->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $primaryKey = $this->primaryKey;
            $delWebhook = Webhook::where('organization_id', $this->organization_id)
                ->where($primaryKey, $id)->delete();

            if (!$delWebhook) {
                $response['status'] = false;
                $response['message'] = env('DEL_FAILED');

                return $response;
            }

            $response['status'] = true;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($id)
    {
    }
}

<?php

namespace App\Http\Infrastructurs\Repositories;

use App\Http\Infrastructurs\Interfaces\RepositoryInterface;
use App\Http\Models\User;
use App\Http\Models\Organization;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use Carbon\Carbon;

class UserRepository implements RepositoryInterface
{
    public $primaryKey;
    protected $organization_id;

    use TrackableTrait;

    public function __construct($organization_id = null)
    {
        $user = new User();
        $this->primaryKey = $user->getKeyName();
        $this->organization_id = $organization_id;
    }

    public function primaryKey()
    {
        return $this->primaryKey;
    }

    public function findAll($limit = null)
    {
        if (!is_null($limit)) {
            $data = User::where('organization_id', $this->organization_id)->limit($limit)->get();
        } else {
            $data = User::where('organization_id', $this->organization_id)->get();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function findById($id)
    {
        if (is_array($id)) {
            $data = User::where('organization_id', $this->organization_id)
                    ->whereIn($this->primaryKey, $id)->get();
        } else {
            $data = User::where($this->primaryKey, $id)
                    ->where('organization_id', $this->organization_id)->get();
        }

        $response['status'] = true;
        $response['property'] = null;
        $response['collection'] = $data;

        return $response;
    }

    public function create($data)
    {
        /*
         * disini nanti dilampirkan mandatory untuk data User itu apa aja dan di validate sebelum di insert
         */

        try {
            $input = inputToLower($data);

            $rules = [
                'org.organization_id' => 'required',
                'user.email' => 'required',
                'user.password' => 'required',
                'user.full_name' => 'required',
                'user.phone' => 'nullable',
                'location.location_id' => 'nullable',
                'group.group_id' => 'nullable',
                'role.role_id' => 'nullable',
                'user.avatar' => 'nullable',
            ];
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = json_decode($error);

                return $response;
            }

            $findUser = $this->isAvailable($input);

            if (!$findUser) {
                $response['status'] = false;
                $response['message'] = env('DUPLICATE_USER');

                return $response;
            }

            $input['user']['password'] = bcrypt($input['user']['password']);
            $input['status'] = (int) env('STATUS_ACTIVE');
            $input['org']['organization_id'] = $this->organization_id;
            $input = extractInputJson($input);

            $createUser = User::create($input);
            $primaryKey = $this->primaryKey;

            $response['status'] = true;
            $response['property'] = [
                'primary_key' => $primaryKey,
                $primaryKey => $createUser->$primaryKey,
            ];
            $response['model'] = $createUser;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }

        return $insert;
    }

    public function register($data)
    {
        try {
            $rules = [
                'email' => 'required',
                'password' => 'required',
                'full_name' => 'required',
                'organization_id' => 'required',
            ];
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                return $error;

                return false;
            }

            $roleRepo = new RoleRepository();

            $adminId = $roleRepo->findAdminId($data['organization_id']);

            if (!$adminId) {
                $response['status'] = false;
                $response['message'] = env('ROLE_NOT_FOUND').':'.env('ROLE_ADMIN');
            }

            $primaryKey = $this->primaryKey;
            $input = inputToLower($data);
            $input['password'] = $data['password'];
            $input['role_id'] = $adminId;
            $input['status'] = (int) env('STATUS_ACTIVE');

            $insert = User::create($input);
            $token = $insert->createToken(env('TOKEN_NAME'))->accessToken;

            $response['status'] = true;
            $response['property'] = [$primaryKey => $insert->$primaryKey];
            $response['model'] = $insert;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }

        $response['status'] = false;

        return $response;
    }

    public function update($data)
    {
        try {
            $rules = [
                'user_id' => 'required',
                'full_name' => 'nullable',
                'email' => 'nullable',
                'password' => 'nullable',
                'status' => 'nullable',
                'phone' => 'nullable|regex:/(08)[0-9]/|max:15',
                'avatar' => 'nullable',
                'group_id' => 'nullable',
                'role_id' => 'nullable',
                'organization_password' => 'nullable',
                'location_id' => 'nullable',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $model = new User();
            $primaryKey = $model->getKeyName();
            $findUser = User::where($this->primaryKey, $data['user_id'])
                        ->where('organization_id', $this->organization_id)
                        ->first();
            $fillableKeys = is_null($model->getFillable()) ? [] : $model->getFillable();

            if (!$findUser) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            if (!empty($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            }

            if (!empty($data['organization_password'])) {
                $data['password'] = $this->copyPassword($this->organization_id);
            }

            foreach ($fillableKeys as $k => $v) {
                if (isset($data[$v])) {
                    $findUser->$v = $data[$v];
                }
            }

            $save = $findUser->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findUser->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $delUser = User::where($this->primaryKey, $id)
                        ->where('organization_id', $this->organization_id)
                        ->delete();

            if (!$delUser) {
                $response['status'] = false;
                $response['message'] = env('DEL_FAILED');

                return $response;
            }

            $response['status'] = true;

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function softDelete($data)
    {
        try {
            $rules = [
                '_id' => 'required',
                'organization_id' => 'nullable',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $model = new User();
            $primaryKey = $model->getKeyName();
            $findUser = User::where($this->primaryKey, $data[$primaryKey])
                        ->where('organization_id', $this->organization_id)
                        ->first();

            if (!$findUser) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $findUser->status = (int) env('STATUS_DELETE');

            $save = $findUser->save();

            if (!$save) {
                $response['status'] = false;
                $response['property'] = '';
                $response['message'] = env('UPDATE_FAILED');

                return $response;
            }

            $response['status'] = true;
            $response['property'] = $findUser->getAttributes();

            return $response;
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function login($data)
    {
        try {
            //cek user
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'organization_id' => $data['organization_id']])) {
                $userdata = Auth::user();
                $UserArray = User::with('organization')->find($userdata->_id)->toArray();
                $User = User::find($userdata->_id);

                if ($UserArray) {
                    if (!isset($UserArray['organization'])) {
                        $response['status'] = 'unverified';
                        $response['message'] = env('NEED_VERIFY_ACCOUNT');

                        return renderResponse($response, 209);
                    }

                    $token = $User->createToken(env('TOKEN_NAME'))->accessToken;
                    $User->token = $token;
                    $saveUser = $User->save();

                    $response['status'] = true;
                    $response['token'] = $token;
                    $response['full_name'] = $User->full_name;

                    return $response;
                }

                $response['status'] = false;
                $response['message'] = env('LOGIN_FAILED');

                return $response;
            }
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function login2($data)
    {
        try {
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'organization_id' => $data['organization_id']])) {
                $userdata = Auth::user();
                $UserArray = User::with('organization')->find($userdata->_id)->toArray();
                $User = User::find($userdata->_id);
                if ($UserArray) {
                    //return token
                    if (!isset($UserArray['organization'])) {
                        $message = ['message', 'Please verif your account'];

                        return renderResponse($message, 209);
                    }

                    $tokenResult = $User->createToken(env('TOKEN_NAME'));
                    $token = $tokenResult->token;
                    $token->expires_at = Carbon::now()->addWeeks(1);
                    $token->save();

                    return response()->json([
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'JWT',
                        'expires_at' => Carbon::parse(
                            $tokenResult->token->expires_at
                        )->toDateTimeString(),
                    ]);
                }
            }
        } catch (\Exception $e) {
        }
    }

    public function logout($token)
    {
        //hapus token dari database
        try {
            $model = User::where('organization_id', $this->organization_id)
                    ->where('token', $token)
                    ->first();

            if (!$model) {
                return false;
            }

            $model->token = null;
            $model->save();

            return true;
        } catch (\Exception $e) {
            die($e);
        }
    }

    protected function isAvailable($data)
    {
        $findUser = User::where('email', $data['user']['email'])
            ->where('organization_id', $data['org']['organization_id'])
            ->first();

        if ($findUser) {
            return false;
        }

        return true;
    }

    protected function copyPassword($organization_id)
    {
        $findUser = Organization::where('_id', $organization_id)
                    ->where('status', 1)
                    ->first();

        if (!$findUser) {
            return false;
        }

        return $findUser->user['password'];
    }
}

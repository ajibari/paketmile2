<?php

namespace App\Http\Infrastructurs\Traits;

use Illuminate\Http\Request;
use App\Http\Models\User;

trait TrackableTrait
{
    public static function bootTrackableTrait()
    {
        static::retrieved(function ($model) {
        });

        static::creating(function ($model) {
            $request = request();
            $ip_address = $request->ip();

            $model->created_by = 'middleware';
            $model->alamat_ip = $ip_address;
        });

        static::updating(function ($model) {
            $request = request();
            $ip_address = $request->ip();

            $model->updated_by = 'middleware';
            $model->alamat_ip = $ip_address;
        });

        static::deleting(function ($model) {
        });
    }

    public static function saveQuery($input, $request, $model)
    {
    }

    public function header(Request $request)
    {
        return $request->header('tes');
    }

    public function getInfoOrg($token)
    {
        $findOrg = User::with('organization')->where('token', $token)->first();

        if (!$findOrg) {
            return false;
        }

        return $findOrg;
    }
}

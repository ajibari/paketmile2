<?php

namespace App\Http\Controllers;

use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\TaskRepository;
use Illuminate\Http\Request;

class TaskController extends BaseController
{
    use TrackableTrait;

    public function create(Request $request)
    {
        $input = $request->input();
        $taskRepo = new TaskRepository($this->organization_id);
        $col = env('FIRESTORE_COL_TASK');
        $doc = bin2hex(openssl_random_pseudo_bytes(10));

        $input['createdAt'] = date('Y-m-d H:i:s');
        $input['organization_id'] = $this->organization_id;
        $saveData = $taskRepo->create($col, $doc, $input);

        if (!$saveData['status']) {
            $response = ['message' => $groupCreate['message']];

            return renderResponse($response, 209);
        }

        $response['status'] = true;
        $response['taskId'] = $doc;

        return renderResponse($response, 200);
    }

    public function createUnofficial(Request $request)
    {
        $input = $request->input();
        $taskRepo = new TaskRepository();

        $collection = $input['collection'];
        $document = $input['document'];

        unset($input['collection']);
        unset($input['document']);

        $taskCreate = $taskRepo->create2($collection, $document, $input);

        $message = ['message' => 'data saved'];

        return renderResponse($message, 200);
    }

    public function update(Request $request)
    {
        $input = $request->input();
        $taskRepo = new TaskRepository($this->organization_id);
        $input['updatedAt'] = date('Y-m-d H:i:s');
        $updateData = $taskRepo->update($input);

        if (!$updateData['status']) {
            $response = ['message' => env('UPDATE_FAILED')];

            return renderResponse($response, 400);
        }

        $response['status'] = true;
        $response['property'] = $updateData['property'];

        return renderResponse($response, 200);
    }

    public function delete(Request $request)
    {
        $input = $request->input();
        $taskRepo = new TaskRepository($this->organization_id);
        $input['deletedAt'] = date('Y-m-d H:i:s');
        $delData = $taskRepo->delete($input);

        if (!$delData['status']) {
            $response = ['message' => env('DELETE_FAILED')];

            return renderResponse($response, 400);
        }

        $response['status'] = true;

        return renderResponse($response, 200);
    }

    public function read($doc_reff_id)
    {
        $taskRepo = new TaskRepository($this->organization_id);
        $findData = $taskRepo->read($doc_reff_id);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Infrastructurs\Traits\TrackableTrait;
use Illuminate\Http\Request;
use App\Http\Infrastructurs\Repositories\WebhookRepository;

class WebhookController extends BaseController
{
    use TrackableTrait;

    public function create(Request $request)
    {
        $webhookRepo = new WebhookRepository($this->organization_id);

        $input = $request->input();
        $webhookCreate = $webhookRepo->create($input['webhook']);

        if (!$webhookCreate['status']) {
            $response['message'] = $webhookCreate['message'];

            return renderResponse($response, 400);
        }

        $response = [$webhookCreate['property']['primary_key'] => $webhookCreate['property'][$webhookCreate['property']['primary_key']]];

        return renderResponse($response, 200);
    }

    public function delete($id)
    {
        try {
            $webhookRepo = new WebhookRepository($this->organization_id);
            $webhookDel = $webhookRepo->delete($id);

            if (!$webhookDel['status']) {
                return renderResponse($groupDel['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $webhookRepo = new WebhookRepository($this->organization_id);

            $input = $request->input();
            $webhookUpdate = $webhookRepo->update($input['webhook']);

            if (!$webhookUpdate['status']) {
                return renderResponse($webhookUpdate['message'], 200);
            }

            return renderResponse($webhookUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function list()
    {
        $webhookRepo = new WebhookRepository($this->organization_id);
        $findWebhook = $webhookRepo->findAll();

        $response['webhooks'] = [
            'list' => $findWebhook['collection'],
            'total_data' => count($findWebhook['collection']),
        ];

        return renderResponse($response, 200);
    }

    public function deleteAll()
    {
    }
}

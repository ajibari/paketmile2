<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\GroupRepository;

class GroupController extends BaseController
{
    use TrackableTrait;

    protected $token;

    public function create(Request $request)
    {
        $groupRepo = new GroupRepository($this->organization_id);

        $input = $request->input();
        // $data = array_merge($input['group'], ['organization_id' => $this->organization_id]);
        $groupCreate = $groupRepo->create($input['group']);

        if (!$groupCreate['status']) {
            $response = ['message' => $groupCreate['message']];

            return renderResponse($response, 209);
        }

        $response = [$groupCreate['property']['primary_key'] => $groupCreate['property'][$groupCreate['property']['primary_key']]];

        return renderResponse($response, 200);
    }

    public function update(Request $request)
    {
        try {
            $groupRepo = new GroupRepository($this->organization_id);

            $data = $request->input('group');
            $groupUpdate = $groupRepo->update($data);

            if (!$groupUpdate['status']) {
                return renderResponse($groupUpdate['message'], 209);
            }

            return renderResponse($groupUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $groupRepo = new GroupRepository($this->organization_id);
            $groupDel = $groupRepo->delete($id);

            if (!$groupDel['status']) {
                return renderResponse($groupDel['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function read(Request $request)
    {
        $groupRepo = new GroupRepository($this->organization_id);
        $findGroup = $groupRepo->findAll();

        $response['group'] = [
            'list' => $findGroup['collection'],
            'total_data' => count($findGroup['collection']),
        ];

        return renderResponse($response, 200);
    }

    public function readWithLimit(Request $request)
    {
    }
}

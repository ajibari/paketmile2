<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\OrgRepository;
use Illuminate\Support\Facades\Validator;

class OrganizationController extends BaseController
{
    use TrackableTrait;

    public function update(Request $request)
    {
        try {
            $orgRepo = new OrgRepository();

            $data = $request->input('organization');
            $orgUpdate = $orgRepo->update($data);

            if (!$orgUpdate['status']) {
                return renderResponse($orgUpdate['message'], 209);
            }

            return renderResponse($orgUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function changePassword(Request $request)
    {
        try {
            $input = $request->input();

            $rules = [
                'password' => 'required',
            ];

            $validator = Validator::make($input['organization'], $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();
                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $orgRepo = new OrgRepository($this->organization_id);
            $data = $input['organization'];
            $password = $data['password'];

            $orgUpdate = $orgRepo->changePassword($password);

            if (!$orgUpdate['status']) {
                return renderResponse($orgUpdate['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }
}

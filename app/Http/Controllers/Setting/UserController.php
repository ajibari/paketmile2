<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\UserRepository;
use App\Http\Infrastructurs\Repositories\RoleRepository;
use App\Http\Infrastructurs\Repositories\GroupRepository;
use App\Http\Infrastructurs\Repositories\LocationRepository;

class UserController extends BaseController
{
    use TrackableTrait;

    public function create(Request $request)
    {
        $userRepo = new UserRepository($this->organization_id);

        $input = $request->input();

        if (!$this->organization_id) {
            $response['status'] = false;

            return $response;
        }

        $input['org'] = ['organization_id' => $this->organization_id];
        $data = $input;
        $groupCreate = $userRepo->create($data);

        if (!$groupCreate['status']) {
            $response = ['message' => $groupCreate['message']];

            return renderResponse($response, 209);
        }

        $response = [$groupCreate['property']['primary_key'] => $groupCreate['property'][$groupCreate['property']['primary_key']]];

        return renderResponse($response, 200);
    }

    public function read()
    {
        $userRepo = new UserRepository($this->organization_id);
        $findUser = $userRepo->findAll();

        $response['user'] = [
            'list' => $findUser['collection'],
            'total_data' => count($findUser['collection']),
        ];

        return renderResponse($response, 200);
    }

    public function update(Request $request)
    {
        try {
            $userRepo = new UserRepository($this->organization_id);

            $input = $request->input();
            $data = extractInputJson($input);

            $userUpdate = $userRepo->update($data);

            if (!$userUpdate['status']) {
                return renderResponse($userUpdate['message'], 209);
            }

            return renderResponse($userUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $userRepo = new UserRepository($this->organization_id);
            $userDel = $userRepo->delete($id);

            if (!$userDel['status']) {
                return renderResponse($userDel['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function object_list()
    {
        try {
            //get all roles in organization
            $roleRepo = new RoleRepository($this->organization_id);
            $findRole = $roleRepo->findAll();

            //get all groups in organization
            $groupRepo = new GroupRepository($this->organization_id);
            $findGroup = $groupRepo->findAll();

            //get all locations in organization
            $locationRepo = new LocationRepository($this->organization_id);
            $findLocation = $locationRepo->findAll();

            $response['role'] = $findRole['collection'];
            $response['group'] = $findGroup['collection'];
            $response['location'] = $findLocation['collection'];

            return renderResponse($response, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }
}

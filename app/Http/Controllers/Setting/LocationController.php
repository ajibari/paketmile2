<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\LocationRepository;
use Illuminate\Support\Facades\Validator;

class LocationController extends BaseController
{
    use TrackableTrait;

    public function create(Request $request)
    {
        $locationRepo = new LocationRepository($this->organization_id);

        $input = $request->input();

        $validator = Validator::make($input, [
            'location.name' => 'required',
            'location.display_name' => 'nullable',
            'location.description' => 'nullable',
            'location.lng' => 'required',
            'location.lat' => 'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->messages()->toJson();

            return renderResponse($error, 209);
        }

        $data = array_merge($input['location'], ['organization_id' => $this->organization_id]);
        $locationCreate = $locationRepo->create($data);

        if (!$locationCreate['status']) {
            $response = ['message' => $locationCreate['message']];

            return renderResponse($response, 209);
        }

        $response = [$locationCreate['property']['primary_key'] => $locationCreate['property'][$locationCreate['property']['primary_key']]];

        return renderResponse($response, 200);
    }

    public function update(Request $request)
    {
        $input = $request->input();

        try {
            $validator = Validator::make($input, [
                'location._id' => 'required',
                'location.name' => 'required',
                'location.display_name' => 'nullable',
                'location.description' => 'nullable',
                'location.lng' => 'nullable',
                'location.lat' => 'nullable',
                'location.status' => 'nullable',
            ]);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();

                return renderResponse($error, 209);
            }

            $locationRepo = new LocationRepository($this->organization_id);

            $data = $request->input('location');
            $locationUpdate = $locationRepo->update($data);

            if (!$locationUpdate['status']) {
                return renderResponse($locationUpdate['message'], 209);
            }

            return renderResponse($locationUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function read(Request $request)
    {
        $locationRepo = new LocationRepository($this->organization_id);
        $findLocation = $locationRepo->findAll();

        $response['location'] = [
            'list' => $findLocation['collection'],
            'total_data' => count($findLocation['collection']),
        ];

        return renderResponse($response, 200);
    }

    public function delete($id)
    {
        try {
            $locationRepo = new LocationRepository($this->organization_id);
            $locationDel = $locationRepo->delete($id);

            if (!$locationDel['status']) {
                return renderResponse($locationDel['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }
}

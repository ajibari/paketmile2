<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\RoleRepository;
use App\Http\Models\Permission;

class PermissionController extends BaseController
{
    use TrackableTrait;

    public function read(Request $request)
    {
        $roleRepo = new RoleRepository($this->organization_id);
        $findRolePermission = $roleRepo->findAllWithPermission();
        $response['permissions'] = [
            'list' => $findRolePermission['collection'],
            'total_data' => count($findRolePermission['collection']),
        ];

        return renderResponse($response, 200);
    }

    public function update(Request $request)
    {
        try {
            $roleRepo = new RoleRepository($this->organization_id);

            $input = $request->input();
            $permissions_id = $this->readDataUpdate($input['permissions']);
            $data['role']['role_id'] = $input['role']['role_id'];
            $data['permission']['permission_id'] = $permissions_id;

            $input_processed = extractInputJson($data);
            /*
            {
                "role": {
                    "role_id": "5b4c53dd2cccc61ae725dd9c"
                },
                "permission":{
                "permission_id": [
                    "5b46c60f2cccc61d7f1bfa33",
                    "5b46c60f2cccc61d7f1bfa3b",
                    "5b46c60f2cccc61d7f1bfa3f"
                ]
                }
            }
             */
            $roleUpdate = $roleRepo->updateRoleWithPermission($input_processed);

            if (!$roleUpdate['status']) {
                return renderResponse($roleUpdate['message'], 209);
            }

            return renderResponse($roleUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    protected function readDataUpdate($data)
    {
        foreach ($data as $k => $v) {
            $parent_name = $v['name'];

            foreach ($v['permissions'] as $k2 => $v2) {
                if ($v2 === true || $v2 === 'true') {
                    $permission_name[] = $k2.'-'.strtolower($parent_name);
                } elseif ($v2 == false || $v2 === 'false') {
                }
            }
        }

        if (!empty($permission_name)) {
            $findPermission = Permission::whereIn('name', $permission_name)->get();
            foreach ($findPermission as $permission) {
                $permissions[] = $permission->_id;
            }

            return $permissions;
        }

        return null;
    }
}

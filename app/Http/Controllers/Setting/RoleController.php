<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\RoleRepository;

class RoleController extends BaseController
{
    use TrackableTrait;

    protected $token;

    public function create(Request $request)
    {
        $roleRepo = new RoleRepository($this->organization_id);

        $input = $request->input();
        $data = array_merge($input['role'], ['organization_id' => $this->organization_id]);
        $groupCreate = $roleRepo->create($data);

        if (!$groupCreate['status']) {
            $response = ['message' => $groupCreate['message']];

            return renderResponse($response, 209);
        }

        $response = [$groupCreate['property']['primary_key'] => $groupCreate['property'][$groupCreate['property']['primary_key']]];

        return renderResponse($response, 200);
    }

    public function update(Request $request)
    {
        try {
            $roleRepo = new RoleRepository($this->organization_id);

            $data = $request->input('role');
            $groupUpdate = $roleRepo->update($data);

            if (!$groupUpdate['status']) {
                return renderResponse($groupUpdate['message'], 209);
            }

            return renderResponse($groupUpdate['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function delete($id)
    {
        try {
            $roleRepo = new RoleRepository();
            $groupDel = $roleRepo->delete($id);

            if (!$groupDel['status']) {
                return renderResponse($groupDel['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function read(Request $request)
    {
        $roleRepo = new RoleRepository($this->organization_id);
        $findRole = $roleRepo->findAll();

        $response['role'] = [
            'list' => $findRole['collection'],
            'total_data' => count($findRole['collection']),
        ];

        return renderResponse($response, 200);
    }
}

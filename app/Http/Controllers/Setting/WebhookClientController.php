<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use App\Http\Infrastructurs\Repositories\WebhookClientRepository;
use Illuminate\Support\Facades\Validator;

class WebhookClientController extends BaseController
{
    use TrackableTrait;

    public function register(Request $request)
    {
        $webhookclient_repo = new WebhookClientRepository($this->organization_id);

        $input = $request->input();
        $input = inputToLower($input['webhook']);

        $rules = [
            'hook_event_id' => 'required',
            'target_url' => 'required',
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            $response['status'] = false;
            $response['message'] = $error;

            return $response;
        }

        $webhookclient_register = $webhookclient_repo->create($input);

        if (!$webhookclient_register['status']) {
            $response = ['message' => $webhookclient_register['message']];

            return renderResponse($response, 400);
        }

        $response = [$webhookclient_register['property']['primary_key'] => $webhookclient_register['property'][$webhookclient_register['property']['primary_key']]];

        return renderResponse($response, 200);
    }

    public function unregister($id)
    {
        try {
            $webhookclient_repo = new WebhookClientRepository($this->organization_id);
            $webhookclient_unregister = $webhookclient_repo->delete($id);

            if (!$webhookclient_unregister['status']) {
                return renderResponse($webhookclient_unregister['message'], 209);
            }

            return renderResponse(null, 200);
        } catch (\Exception $e) {
            die($e);
        }
    }

    public function list(Request $request)
    {
        $webhookclient_repo = new WebhookClientRepository($this->organization_id);
        $find_webhookclient = $webhookclient_repo->findAll();

        $response['group'] = [
            'list' => $find_webhookclient['collection'],
            'total_data' => count($find_webhookclient['collection']),
        ];

        return renderResponse($response, 200);
    }

    public function update(Request $request)
    {
        try {
            $input = $request->input();

            $rules = [
                '_id' => 'required',
                'hook_event_id' => 'nullable',
                'target_url' => 'nullable',
            ];

            $validator = Validator::make($input['webhook'], $rules);

            if ($validator->fails()) {
                $error = $validator->messages()->toJson();
                $response['status'] = false;
                $response['message'] = $error;

                return $response;
            }

            $webhookclient_repo = new WebhookClientRepository($this->organization_id);

            $data = $request->input('webhook');
            $webhookclient_update = $webhookclient_repo->update($data);

            if (!$webhookclient_update['status']) {
                return renderResponse($webhookclient_update['message'], 209);
            }

            return renderResponse($webhookclient_update['property'], 200);
        } catch (\Exception $e) {
            die($e);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Infrastructurs\Repositories\UserRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Infrastructurs\Traits\TrackableTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\User;
use Lcobucci\JWT\Parser;
use App\Http\Infrastructurs\Repositories\OrgRepository;
use App\Http\Infrastructurs\Repositories\RoleRepository;

class TesController extends BaseController
{
    use TrackableTrait;

    public function index(Request $request)
    {
        dd($request->input());
    }

    public function get()
    {
        $userRepo = new UserRepository();

        $data = ['name' => 'Bariss', 'birthdate' => '2019-01-10', 'gender' => 'male'];

        $validator = Validator::make($data, [
            'name' => 'required',
            'birthdate' => 'required',
            'gender' => 'required',
        ]);

        if ($validator->fails()) {
            return renderResponse('ERROR VALIDATE');
        }

        $token = $userRepo->create($data);
        $success['token'] = $token;

        return response()->json(['success' => $success], 200);

        return renderResponse('tes');

        return;

        return renderResponse($userRepo->findAll(10));
    }

    public function getLogin(Request $request)
    {
        $tokenRaw = $request->bearerToken();
        $parser = new Parser();
        $token = $parser->parse($tokenRaw)->getHeader('jti');
        // $data = User::where('id', );
        dd($token);
        dd(Auth::check());
        dd($data);
    }

    public function org_create(Request $request)
    {
        $orgRepo = new OrgRepository();

        $input = $request->input();

        $pass = $input['user']['password'];
        unset($input['user']['password']);
        $input = inputToLower($input);
        $input['status'] = 0;
        $input['user']['password'] = bcrypt($pass);
        $orgCreate = $orgRepo->create($input);

        $primaryKey_org = $orgRepo->primaryKey();

        if ($orgCreate['status'] === false) {
            $message = ['message' => $orgCreate['message']];

            return renderResponse($message, 400);
        }

        $response = [$primaryKey_org, $orgCreate['property'][$primaryKey_org]];

        return renderResponse($response, 200);
    }

    public function cekAmbilDataJson()
    {
        $orgRepo = new OrgRepository();
        dd($orgRepo->verification('bari@gmail.com', 'slkcnm9cepuwn'));
    }

    public function cek()
    {
        return renderResponse('welcome to API GATEWAY', 200);
    }

    public function org_check(Request $request)
    {
        $input = $request->input();
        $orgRepo = new OrgRepository();

        $data = $orgRepo->isAvailable($input);

        if ($data['status'] === 'error') {
            $message = ['message' => json_decode($data['message'])];

            return renderResponse($message, 209);
        } elseif (!$data['status']) {
            $message = ['message' => $data['message']];

            return renderResponse($message, 209);
        }

        return renderResponse(null, 200);
    }

    public function org_login(Request $request)
    {
        $input = $request->input();
        $orgRepo = new OrgRepository();

        $data = $orgRepo->isValid($input);

        if (!$data) {
            $message = ['message' => env('ORG_NOT_FOUND')];

            return renderResponse(null, 209);
        }

        return renderResponse(null, 200);
    }

    public function registerUnused(Request $request)
    {
        $userRepo = new UserRepository();

        $data = $request->input();

        $validator = Validator::make($data, [
            'org_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'full_name' => 'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->messages()->toJson();

            return renderResponse($error, 209);
        }

        $data['password'] = bcrypt($data['password']);

        $token = $userRepo->register($data);
        $success['token'] = $token;

        return renderResponse([$userRepo->primaryKey() => $token], 200);
    }

    public function verification(Request $request)
    {
        //cek token dan email untuk verifikasi dan hanya bisa dilakukan sekali saja
        $orgRepo = new OrgRepository();
        $userRepo = new UserRepository();

        $input = $request->input();

        $validator = Validator::make($input, [
            'user.email' => 'required|email',
            'user.activation_code' => 'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->messages()->toJson();

            return renderResponse($error, 209);
        }

        $input = inputToLower($input);
        $findOrg = $orgRepo->verification($input['user']['email'], $input['user']['activation_code']);

        if ($findOrg) {
            $primaryKey_org = $orgRepo->primaryKey();

            $roleRepo = new RoleRepository($findOrg[$primaryKey_org]);

            $data = [$primaryKey_org => $findOrg[$primaryKey_org], 'user' => ['activation_code' => 'verified'], 'status' => (int) env('STATUS_ACTIVE')];
            $save = $orgRepo->update($data);
            unset($save['property']['user']['activation_code']);
            $primaryKey_user = $userRepo->primaryKey();
            $save['property']['user']['organization_id'] = $findOrg[$primaryKey_org];

            $createDefaultRole = $roleRepo->createDefaultRole();
            $saveUser = $userRepo->register($save['property']['user']);

            if (!$saveUser) {
                return renderResponse($response, 400);
            }

            return renderResponse(null, 200);
        }

        $message = ['message' => 'unknown request'];

        return renderResponse($message, 209);
    }

    public function login3(Request $request)
    {
        $input = $request->input();
        if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
            $userdata = Auth::user();
            $user = User::find($userdata->_id);

            $data['token'] = $user->createToken('nApp')->accessToken;
            $data['message'] = 'success';
            $data['status'] = 1;
            $data['loginFlag'] = false;

            return response()->json($data, 200);
        } else {
            return response()->json(['message' => 'Password or username didn\'t match', 'status' => 0, 'loginFlag' => true], 200);
        }
    }

    public function login(Request $request)
    {
        $input = $request->input();

        $userRepo = new UserRepository();
        $orgRepo = new OrgRepository();

        $validator = Validator::make($input, [
            'org_name' => 'required',
            'user.email' => 'required|email',
            'user.password' => 'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->messages()->toJson();

            return renderResponse($error, 209);
        }

        //cek organization name
        $org = $orgRepo->findByName(strtolower($input['org_name']));

        if (!$org) {
            $message = ['message' => env('ORG_NOT_FOUND')];

            return renderResponse($message, 209);
        }

        $primaryKey_org = $orgRepo->primaryKey();
        $idOrg = $org->$primaryKey_org;

        $input['organization_id'] = $idOrg;

        $password = $input['user']['password'];
        unset($input['user']['password']);
        $input = inputToLower($input);
        $input['user']['password'] = $password;

        $data = ['organization_id' => $idOrg, 'email' => $input['user']['email'], 'password' => $input['user']['password']];
        $userLogin = $userRepo->login($data);

        if (!$userLogin['status']) {
            return renderResponse($userLogin['message'], 209);
        }

        $response = ['token' => $userLogin['token'], 'full_name' => $userLogin['full_name']];

        return renderResponse($response, 200);
    }

    public function login2(Request $request)
    {
        $input = $request->input();

        $userRepo = new UserRepository();
        $orgRepo = new OrgRepository();

        //cek organization name
        $org = $orgRepo->findByName(strtolower($input['org_name']));

        if (!$org) {
            $message = ['message' => env('ORG_NOT_FOUND')];

            return renderResponse($message, 209);
        }

        $primaryKey_org = $orgRepo->primaryKey();
        $idOrg = $org->$primaryKey_org;

        $input['organization_id'] = $idOrg;

        $data = ['organization_id' => $idOrg, 'email' => $input['user']['email'], 'password' => $input['user']['password']];
        $userLogin = $userRepo->login2($data);

        return $userLogin;
        if (!$userLogin) {
            $message = ['message' => env('LOGIN_FAILED')];

            return renderResponse($message, 209);
        }

        $response = ['token' => $userLogin];

        return renderResponse($response, 200);
    }

    public function logout(Request $request)
    {
        $input = $request->input();
        $header = $request->header('X-Auth-Key');

        $userRepo = new UserRepository($this->organization_id);
        $logout = $userRepo->logout($header);

        if ($logout) {
            return renderResponse(null, 200);
        }

        $message = ['message', env('LOGOUT_ERROR')];

        return renderResponse($message, 209);
    }

    public function tes(Request $request)
    {
        $input = $request->input();
        dd($request->header('tes'));
        echo 'tes aja';
    }

    public function tesHasMany()
    {
        $userRepo = new UserRepository();
        $result = $userRepo->tes();

        // dd($result);

        return renderResponse($result, 200);
    }

    public function roleTES()
    {
        $roleRepo = new RoleRepository();
        $result = ($roleRepo->findAdminId());

        return renderResponse($result, 200);
    }
}

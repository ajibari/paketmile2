<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;

class BaseController extends Controller
{
    public $organization_id;

    public function __construct(Request $request)
    {
        $tokenVar = env('HEADER_AUTH');
        $token = $request->header($tokenVar);

        $findUser = User::where('token', $token)->first();

        if (!$findUser) {
            $this->organization_id = 'undefined';

            return $this->organization_id;
        }

        $this->organization_id = $findUser->organization_id;

        return $this->organization_id;
    }
}

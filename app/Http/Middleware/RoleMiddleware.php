<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $roles, $options = '')
    {
        $role = trim($roles);
        $roles = explode('|', $role);
        $token = $request->header('X-Auth-Key');

        return $next($request);
    }
}

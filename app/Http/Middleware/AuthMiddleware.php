<?php

namespace App\Http\Middleware;

use App\Http\Models\User;
use Closure;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions, $options = '')
    {
        $permission = trim($permissions);
        $permissions = explode('|', $permission);
        $token = $request->header('X-Auth-Key');

        if (empty($token)) {
            return renderResponse(null, 401);
        }

        $user = User::where('token', $token)->first();

        if (!empty($user)) {
            return $next($request);
        } else {
            return renderResponse(null, 401);
        }
    }
}

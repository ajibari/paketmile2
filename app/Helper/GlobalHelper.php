<?php


function renderResponse($result = null, $status)
{
    return response()->json($result, $status);
}

function inputToLower($input)
{
    foreach ($input as $k => $v) {
        if (is_array($v)) {
            foreach ($v as $k2 => $v2) {
                if (is_array($v2)) {
                    $input[$k][$k2] = inputToLower($v2);
                } else {
                    $input[$k][$k2] = strtolower($v2);
                }
            }
        } else {
            $input[$k] = strtolower($v);
        }
    }

    return $input;
}

function extractInputJson($input)
{
    $result = [];
    foreach ($input as $k => $v) {
        if (is_array($v)) {
            foreach ($v as $k2 => $v2) {
                $result[$k2] = $v2;
            }
        } else {
            $result[$k] = $v;
        }
    }

    return $result;
}

function isSameOrganization($token)
{
}
